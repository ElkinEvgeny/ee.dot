﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace EE.Dot.Task2.Service.Controllers
{
    [Route("api/vote")]
    public class VoteController : Controller
    {
        private static Dictionary<string, Guid> _users = new Dictionary<string, Guid>();
        private static Dictionary<Guid, bool> _voters = new Dictionary<Guid, bool>();
        private static Dictionary<string, int> _candidates = new Dictionary<string, int>
        {
            {"Путин", 1},
            {"Жириновский", 2},
            {"Новальный", 3}
        };
        private static Dictionary<int, int> _results = new Dictionary<int, int>();

        [HttpGet("register")]
        public JsonResult Register(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = "Представьтесь"
                });
            }
            if (_users.ContainsKey(name))
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = "Вы уже голосовали"
                });
            }
            var guid = Guid.NewGuid();
            _voters.Add(guid, false);
            _users.Add(name, guid);
            return Json(new
            {
                IsSuccess = true,
                Id = guid
            });
        }

        [HttpGet("candidates")]
        public JsonResult GetCandidates()
        {
            return Json(new {IsSuccess = true, Items = _candidates});
        }

        [HttpGet("vote")]
        public JsonResult vote([FromQuery]Guid user, int candidateId)
        {
            if (_candidates.All(x => x.Value != candidateId))
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = "Данного кадидата не существует"
                });
            }
            if (!_voters.ContainsKey(user) || _voters[user] == true)
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = "Вы не можете голосовать"
                });
            }
            if (_results.ContainsKey(candidateId))
            {
                _results[candidateId]++;
            }
            else
            {
                _results[candidateId] = 1;
            }
            _voters[user] = true;
            return Json(new { IsSuccess = true });
        }

        [HttpGet("results")]
        public JsonResult GetResults()
        {
            return Json(new
            {
                IsSuccess = true,
                Items = _candidates.Select(x => new
                {
                    Name = x.Key,
                    Votes = _results.ContainsKey(x.Value)
                         ? _results[x.Value]
                         : 0
                })
            });
        }
    }
}
