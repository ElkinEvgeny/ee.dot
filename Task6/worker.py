import zmq

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.connect("tcp://localhost:5560")
print("worker live..")
while True:
	message = socket.recv()
	try:
		path = "./wwwroot" + message.decode()
		print("search file: " + path)
		file = open(path,'r')
		html = file.read()
		socket.send_string(html)
		print(html)
	except:
		print("File not found")
		socket.send_string("file_not_found")