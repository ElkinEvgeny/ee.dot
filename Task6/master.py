import zmq
import socket

context = zmq.Context()
zmqsocket = context.socket(zmq.REQ)
zmqsocket.connect("tcp://localhost:5559")
sock = socket.socket()
sock.bind(("", 5000))
sock.listen(10)
print ("listen: http://localhost:5000")
while True:
	conn, addr = sock.accept()
	data = conn.recv(16384).decode()
	lines = data.split("\r\n") 
	try:
		if lines[0] != "":
			path = lines[0].split()[1];
			print("request: " +  path)
			zmqsocket.send(path.encode())
			html = zmqsocket.recv()
			print("response: " + html.decode())
			conn.send(b'\r\n')
			conn.send(b'HTTP/1.1 200 OK\r\n')
			conn.send(b'Content-Type: text/html\r\n')
			conn.send(b'\r\n')
			conn.send(html)
			conn.send(b'\r\n')
	finally:
		conn.close()