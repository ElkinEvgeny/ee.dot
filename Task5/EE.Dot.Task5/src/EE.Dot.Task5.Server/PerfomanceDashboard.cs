﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EE.Dot.Task5.Server
{
    public class PerfomanceDashboard
    {
        private readonly Dictionary<string, PerfomanceModel> _storage = new Dictionary<string, PerfomanceModel>();
        private readonly string _metric;

        public PerfomanceDashboard(string metric)
        {
            _metric = metric;
        }

        public void Tick(string machine, string value)
        {
            if (!_storage.ContainsKey(machine))
            {
                var model = new PerfomanceModel(machine, value);
                _storage.Add(machine, model);
            }
            else
            {
                var model = _storage[machine];
                model.Save(value);
            }
        }

        public void Print()
        {
            Console.Clear();
            Console.WriteLine($"=====Use {_metric}=====");
            foreach (var model in _storage.Values)
            {
                Console.WriteLine($"Machine: {model.Name}");
                Console.WriteLine($"Avg. value: {(model.Sum/model.Queue.Count()):f2}");
                Console.WriteLine($"Current value: {model.Current :f2}");
                var values = new int[10];
                model.Queue.CopyTo(values, 0);
                PlotPrinter.PrintPlot(values);
            }
        }

        private class PerfomanceModel
        {
            public string Name { get; set; }
            public int Sum { get; set; }
            public int Current { get; set; }
            public Queue<int> Queue { get; set; }

            public PerfomanceModel(string name, string value)
            {
                Name = name;
                Sum = int.Parse(value);
                Current = Sum;
                Queue = new Queue<int>(Sum);
            }

            public void Save(string value)
            {
                if (Queue.Count == 10)
                {
                    var old = Queue.Dequeue();
                    Sum -= old;
                }
                var next = int.Parse(value);
                Current = next;
                Sum += next;
                Queue.Enqueue(next);

            }
        }
    }
}