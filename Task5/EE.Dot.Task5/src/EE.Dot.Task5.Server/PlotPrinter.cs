using System;
using System.Linq;

namespace EE.Dot.Task5.Server
{
    public static class PlotPrinter
    {
        private static int _height = 20;
        private static int _width = 50;

        public static void PrintPlot(int[] percents)
        {

            var lines = new string[_height];
            for (var i = 0; i < _height; i++)
            {
                lines[i] = string.Empty;
            }
            for (var column = 0 ; column < percents.Length; column++)
            {
                var percent = percents[column];
                for (var row = 0; row < _height; row++)
                {
                    if (percent > row * 100 / _height)
                    {
                        lines[row]+= "00000";
                    } else
                    {
                        lines[row] += "     ";
                    }
                }
            }
            Console.WriteLine(new string('I', _width + 2));
            foreach (var line in lines.Reverse())
            {
                Console.Write("I");
                Console.Write(line);
                Console.WriteLine("I");
            }
            Console.WriteLine(new string('I', _width + 2));
        }
    }
}