﻿using System;
using System.Linq;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace EE.Dot.Task5.Server
{
    public class Program
    {
        private static PerfomanceDashboard _dashboard;
        private static readonly string[] _metrics = {"CPU", "RAM", "DISK", "NET"};

        public static void Main(string[] args)
        {
            if (args.Length != 1 || !_metrics.Any(x => x.Equals(args[0])))
            {
                Console.WriteLine("Plsease set metrics: [CPU/RAM/DISK/NET]");
                Environment.Exit(1);
            }

            var metric = args[0];
            _dashboard = new PerfomanceDashboard(metric);
            var factory = new ConnectionFactory { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(
                    exchange: "perfomance",
                    type: "topic");
                var queueName = channel.QueueDeclare().QueueName;

                channel.QueueBind(
                    queue: queueName,
                    exchange: "perfomance",
                    routingKey: $"RAM.*");

                Console.WriteLine($"Listening {metric} perfomance...");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += Accept;

                channel.BasicConsume(
                    queue: queueName,
                    noAck: true,
                    consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        public static void Accept(object model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var routingKey = ea.RoutingKey;
            var machine = string.Join(".", routingKey.Split('.').Skip(1));
            var message = Encoding.UTF8.GetString(body);
            _dashboard.Tick(machine, message);
            _dashboard.Print();
        }
    }
}
