﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;

namespace EE.Dot.Task5.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Please set machine");
                Environment.Exit(1);
            }

            var factory = new ConnectionFactory { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "perfomance", type: "topic");
                var autoEvent = new AutoResetEvent(false);
                var monitor = new PerfomanceMonitor(channel, args[0]);
                var timer = new Timer(monitor.Process, autoEvent, 0, 500);

                try
                {
                    autoEvent.WaitOne();
                }
                finally
                {
                    Console.WriteLine("Monitoring complete!");
                    timer.Dispose();
                }
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }

    public class PerfomanceMonitor
    {
        private static readonly string[] Metrics = { "CPU", "RAM", "DISK", "NET" };
        private readonly Random _rand = new Random();

        private readonly IModel _channel;
        private readonly string _machine;

        public PerfomanceMonitor(IModel channel, string machine)
        {
            _channel = channel;
            _machine = machine;
        }

        public void Process(Object stateInfo)
        {
            foreach (var metricName in Metrics)
            {
                var metric = _rand.Next(101);
                var body = Encoding.UTF8.GetBytes(metric.ToString());

                _channel.BasicPublish(exchange: "perfomance", routingKey: $"{metricName}.{_machine}", basicProperties: null, body: body);
                Console.WriteLine($" [x] Perfomance ({metricName}): {metric}");
            }
        }
    }
}
