﻿using System;
using System.Net;
using System.Net.Http;
using CsQuery;
using ogp;
using Thrift.Server;
using Thrift.Transport;

namespace EE.DOT.Task1.Server
{
    class Program
    {
        public class ParserHandler : OpenGraphParser.Iface
        {
            public ResponseData getOpenGraphData(RequestData data)
            {
                try
                {
                    var uri = new Uri(data.Url);
                    var client = new HttpClient();
                    var response = client.GetAsync(uri).Result;
                    if (response.StatusCode == HttpStatusCode.Redirect)
                    {
                        return getOpenGraphData(new RequestData {Url = response.Headers.Location.AbsoluteUri});
                    }

                    var html = response.Content.ReadAsStringAsync().Result;

                    var result = new OpenGraphData();
                    var isEmpty = true;
                    var cq = new CQ(html);
                    foreach (var meta in cq.Find("meta"))
                    {
                        var name = meta.GetAttribute("property");
                        var value = meta.GetAttribute("content");
                        switch (name)
                        {
                            case "og:title":
                                result.Title = value;
                                isEmpty = false;
                                break;
                            case "og:type":
                                result.Type = value;
                                isEmpty = false;
                                break;
                            case "og:image":
                                result.Image = value;
                                isEmpty = false;
                                break;
                            case "og:url":
                                result.Url = value;
                                isEmpty = false;
                                break;
                        }
                    }
                    if (!isEmpty)
                    {
                        return new ResponseData
                        {
                            Status = OpenGraphStatus.Exists,
                            Data = result
                        };
                    }
                    return new ResponseData
                    {
                        Status = OpenGraphStatus.NotExists
                    };
                }
                catch (UriFormatException)
                {
                    throw new ConnectionException
                    {
                        Message = $"Bad uri format: {data.Url}"
                    };
                }
                catch (Exception ex)
                {
                    throw new ConnectionException
                    {
                        Message = $"Can't extract open graph data from url: {data.Url}({ex.Message})"
                    };
                }
            }
        }

        static void Main(string[] args)
        {
            try
            {
                var handler = new ParserHandler();
                OpenGraphParser.Processor processor = new OpenGraphParser.Processor(handler);
                TServerTransport serverTransport = new TServerSocket(9090);
                TServer server = new TSimpleServer(processor, serverTransport);

                Console.WriteLine("Listening...");
                server.Serve();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("done.");
        }
    }
}
