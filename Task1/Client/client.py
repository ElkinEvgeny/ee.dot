import sys
sys.path.append('Generated')

from ogp import OpenGraphParser
from ogp.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

def main():
	transport = TSocket.TSocket('localhost', 9090)
	transport = TTransport.TBufferedTransport(transport)
	protocol = TBinaryProtocol.TBinaryProtocol(transport)
	client = OpenGraphParser.Client(protocol)

	try:
		transport.open()
		print("connect...")
		
		while(True):
			url = input("Plase input url...\r\n")
			
			if url == "":
				break
			
			requestData = RequestData()
			requestData.url = url
			responseData = client.getOpenGraphData(requestData)
			if (responseData.status == OpenGraphStatus.Exists):
				print('Title:' + responseData.data.title)
				print('Type:' + responseData.data.type)
				print('Image:' + responseData.data.image)
				print('Url:' + responseData.data.url)
			else:
				print("open graph data not found")
			print()
	except TTransport.TTransportException:
		print("Can't connect to server")
	except ConnectionException as e:
		print("Can't connect to url: " + e.message)
	finally:
		print("Closing...")
		transport.close()
	
main()