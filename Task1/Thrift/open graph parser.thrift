namespace py ogp
namespace csharp ogp


struct RequestData {
  1: string url
}

enum OpenGraphStatus {
	Exists = 1,
	NotExists = 2
}

struct OpenGraphData {
  1: string title,
  2: string type,
  3: string image,
  4: string url,
}

struct ResponseData {
  1: OpenGraphStatus status,
  2: optional OpenGraphData data
}

exception ConnectionException {
  1: string message
}

service OpenGraphParser {
   ResponseData getOpenGraphData(1:RequestData data) throws (1:ConnectionException err)
}

